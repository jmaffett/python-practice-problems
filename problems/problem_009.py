# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    letter_list = []
    for letter in word:
        letter_list.append(letter)
    word_in_reverse_list = letter_list[::-1]

    if word_in_reverse_list == letter_list:
        return True
    else:
        return False

word = "banana"
is_it_a_pal = bool(is_palindrome(word))
if is_it_a_pal == True:
    print("Word is a palindrome")
else:
    print("Word is NOT a palindrome")
