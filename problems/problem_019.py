# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    if (
        x >= rect_x
        and x <= rect_x + rect_width
        and y >= rect_y
        and y <= rect_y + rect_height
    ):
       return "Yes"
    else:
        return "No"

x = 7
y= 7
x_start = 4
y_start = 9
width = 2
height = 6

result = is_inside_bounds(x, y, x_start, y_start, width, height)
print(result)
