# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html

import random

def generate_lottery_numbers():
    list_of_six_random_numbers = []
    numbers_to_be_generated = 7
    for num in range(numbers_to_be_generated):
        to_add = random.randint(1, 40)
        if to_add not in list_of_six_random_numbers:
            list_of_six_random_numbers.append(to_add)
        else:
            numbers_to_be_generated += 1
    return list_of_six_random_numbers

print(generate_lottery_numbers())
