# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    digits_in_number = 0
    num_as_str = str(number)
    for c in num_as_str:
        if c.isdigit():
            digits_in_number += 1
    length = length - digits_in_number

    return (pad * length + num_as_str)


test = pad_left(19, 5, " ")

print(test)
