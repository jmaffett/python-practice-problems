# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

def sum_fraction_sequence(input):
    sum_fractions = 0
    for num in range(input + 1):
        numerator = num
        denominator = num + 1
        sum_fractions += numerator / denominator
    return sum_fractions


result = 1/2
test = 1
print(result)
print(sum_fraction_sequence(test))
