# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age>= 18 or has_consent_form == True:
        return 1
    else:
        return 0


age = 13
has_consent_form = True

can_skydive = bool(can_skydive(age, has_consent_form))

if can_skydive == True:
    print("Congrats you can skydive!")
else:
    print("Sorry you no fly :(")
