# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    first_half = []
    second_half = []
    if len(list) % 2 == 0:
        half = int(len(list)/2)
    else:
        half = int(len(list)/2) + 1
    for num in range(0, half):
        first_half.append(list[num])
    for num in range((half), len(list)):
        second_half.append(list[num])
    return first_half, second_half


input = [1, 2, 3, 4, 5, 6]
print(halve_the_list(input))
