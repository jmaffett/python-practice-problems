# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <= 1:
        return None
    sorted_list= sorted(values)
    sorted_reversed = sorted_list[::-1]
    for i in range(len(sorted_reversed)):
        if sorted_reversed[i] < sorted_reversed[i-1]:
            return sorted_reversed[i]

numbers_list = []

print("Second largest is: ", find_second_largest(numbers_list))
