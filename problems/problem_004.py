# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    nums = [value1, value2, value3]
    greatest = 0
    for i in range(len(nums)):
        if nums[i]>= greatest:
            greatest = nums[i]
        else:
            greatest = greatest

    return greatest

v1 = 34.02
v2 = 34.0003
v3 = 34.002

max = max_of_three(v1, v2, v3)
print(max)
