# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower = False
    upper = False
    digit = False
    special_char = False
    special_char_lst = ["$", "!", "@"]
    if len(password) > 6 and len(password) < 12:
        for char in password:
            if char.isalpha():
                if char.islower():
                    lower = True
                if char.isupper():
                    upper = True
            elif char.isdigit():
                digit = True
            elif char in special_char_lst:
                special_char = True
        if (
            lower == True
            and upper == True
            and digit == True
            and special_char == True
        ):
            return "Password accepted!"
        else:
            return "Password not accepted"
    else:
        return "Password not long enough"

test = "ILOVE3GODS!"
print(check_password(test))
